package hello.world.app;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SayHelloController {

	@GetMapping(path = "/{name}")
	public String sayHelloTo(@PathVariable("name") String name) {
		return String.format("<h1>Hello %s!</h1>", name);
	}
}
